const Chat = require("../models/Chat");
const Mensaje = require('../models/Mensaje');
let _ = require('lodash');

let chats = {};

function error(mensaje) {
  console.log(mensaje);
}

function obtenerNombresChat() {
  return Object.values(chats);
}

let eventosChat = (socket) => {
  socket.emit('recibirChats', obtenerNombresChat());

  socket.on('crearChat', data => {
    let aleatorio = Math.floor(Math.random() * 1000000) + 1000;
    let aleatorio2 = Math.floor(Math.random() * 1000000) + 1000;

    let nombreChat = data.chatName ? data.chatName : `new chat${aleatorio}`;
    let nombreUsuario = data.userName ? data.userName : `new user${aleatorio2}`;

    let chat = new Chat(nombreChat);
    socket.join(data.chatName);
    socket.emit("nombreUsuario", nombreUsuario);
    console.log(socket.rooms)
    if (chats[chat.nombre]) {
      //ya existe un chat con ese nombre
      error(`Ya existe un chat con el nombre ${chat.nombre}`);
    } else {
      chats[chat.nombre] = chat;
      console.log(`Se creó el chat ${chat.nombre} con exito`);
      socket.emit('recibirChats', obtenerNombresChat());
      socket.broadcast.emit('recibirChats', obtenerNombresChat());
    }
  })

  socket.on("enviarMensaje", data => {
    //  console.log(socket.rooms )
    let mensaje = new Mensaje(data.userName, data.texto);
    _.forEach(socket.rooms, (key, value) => {
      let chat = chats[value];
      if (chat) {
        chat.mensajes.push(mensaje);
        socket.broadcast.to(value).emit("recibirMensaje", {
          userName: mensaje.usuario,
          fecha: mensaje.fecha,
          texto: mensaje.texto
        });
      }
    })

  })

  socket.on("salirChat", data => {
    socket.leave(data.chatName);
  })

  socket.on('unirseChat', data => {

    let aleatorio = Math.floor(Math.random() * 1000000) + 1000;
    let nombreUsuario = data.userName ? data.userName : `new user${aleatorio}`;
    console.log(`usuario ${nombreUsuario} uniendose al char ${data.chatName}`)
    socket.emit("nombreUsuario", nombreUsuario);
    socket.join(data.chatName);

    if (!data.chatName || !chats[data.chatName])
      error("Ingrese un chat valido");
    //  chats.get(data.chatName).agregarUsuario(usuario);
    let chat = chats[data.chatName];
    chat.mensajes.forEach(mensaje => {
      console.log(mensaje)

      socket.emit("recibirMensaje", {
        userName: mensaje.usuario,
        fecha: mensaje.fecha,
        texto: mensaje.texto
      });
    })
  })

}

module.exports = eventosChat;
