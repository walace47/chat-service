# Chat FAI

### Installation

La aplicacion requiere [Node.js](https://nodejs.org/)

Instalar dependencias y ejecutar el servidor
```sh
$ cd chatService
$ npm install
$ node app
```
acceder a
```
http://localhost
```
En la carpeta public hay un cliente de prueba programado en angular el cliente apunta a localhost para cambiar la direccion de ip modificar las variables de ambiente y compilar este proyecto [ClienteAngular](https://gitlab.com/walace47/chat-client)