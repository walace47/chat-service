var express = require('express')
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var eventos = require('./funcionalidades/funcionalidadChat');
var cors = require('cors');
app.use(express.static(__dirname+'/public'));

app.use(cors);
io.on('connection', eventos);
server.listen(80);
